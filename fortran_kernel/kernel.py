import base64
import imghdr
import io
import os
import sys
import traceback

from ipykernel.kernelbase import Kernel

from .utils import stdout_redirector

from lfortran.ast import dump, SyntaxErrorException
from lfortran.semantic.analyze import SemanticError
from lfortran.codegen.evaluator import FortranEvaluator

class FortranKernel(Kernel):
    implementation = 'LFortran'
    implementation_version = '1.0'
    language = 'no-op'
    language_version = '0.1'
    language_info = {
        'name': 'fortran',
        'mimetype': 'text/x-fortran',
        'file_extension': '.f90',
        'version': '0.1',
    }
    banner = "Fortran kernel (lfortran)"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.fe = FortranEvaluator()

    def do_execute(self, code, silent, store_history=True, user_expressions=None,
                   allow_stdin=False):

        showllvm = False
        showasm = False
        showast0 = False

        stdout_buf = io.BytesIO()
        if code.startswith(r"%ls"):
            with stdout_redirector(stdout_buf):
                os.system("ls -l")
            result = None
        elif code.startswith(r"%showimage"):
            filename = code.split()[1]
            with open(filename, 'rb') as f:
                image = f.read()
            image_type = imghdr.what(None, image)
            if image_type is None:
                raise ValueError("Not a valid image: %s" % image)
            image_data = base64.b64encode(image).decode('ascii')
            self.send_response(self.iopub_socket, 'display_data', {
                'data': {
                    'image/' + image_type: image_data
                },
                'metadata': {},
            })
            result = None
        else:
            if code.startswith(r"%%showllvm"):
                code = code[code.find("\n")+1:]
                showllvm = True
            if code.startswith(r"%%showasm"):
                code = code[code.find("\n")+1:]
                showasm = True
            if code.startswith(r"%%showast"):
                code = code[code.find("\n")+1:]
                showast0 = True
            try:
                with stdout_redirector(stdout_buf):
                    result = self.fe.evaluate(code)
            except SyntaxErrorException as e:
                error_content = {
                    'execution_count': self.execution_count,
                    'ename': '',
                    'evalue': '2',
                    'traceback': [
                        str(e),
                    ],
                }
                self.send_response(self.iopub_socket, 'error', error_content)
                error_content['status'] = 'error'
                return error_content
            except SemanticError as e:
                error_content = {
                    'execution_count': self.execution_count,
                    'ename': '',
                    'evalue': '3',
                    'traceback': [
                        str(e),
                    ],
                }
                self.send_response(self.iopub_socket, 'error', error_content)
                error_content['status'] = 'error'
                return error_content
            except Exception:
                tr = traceback.format_exc()
                error_content = {
                    'execution_count': self.execution_count,
                    'ename': '',
                    'evalue': '1',
                    'traceback': [
                        tr,
                        "",
                        "This is an internal error in fortran_kernel, " \
                        "please send a bug report.",
                    ],
                }
                self.send_response(self.iopub_socket, 'error', error_content)
                error_content['status'] = 'error'
                return error_content

        if showllvm:
            text = self.fe._source_ll_opt
            assert isinstance(text, str)
            text = "LLVM IR (optimized):\n" + text
            self.send_response(self.iopub_socket, 'stream', {
                'name': 'stdout',
                'text': text
            })
        if showasm:
            text = self.fe._source_asm
            assert isinstance(text, str)
            text = "ASM:\n" + text
            self.send_response(self.iopub_socket, 'stream', {
                'name': 'stdout',
                'text': text
            })
        if showast0:
            text = "Parse AST:\n" + dump(self.fe._ast_tree0)
            self.send_response(self.iopub_socket, 'stream', {
                'name': 'stdout',
                'text': text
            })

        if code.endswith(r"show()"):
            filename = "show_output%d.png" % result
            with open(filename, 'rb') as f:
                image = f.read()
            image_type = imghdr.what(None, image)
            if image_type is None:
                raise ValueError("Not a valid image: %s" % image)
            image_data = base64.b64encode(image).decode('ascii')
            self.send_response(self.iopub_socket, 'display_data', {
                'data': {
                    'image/' + image_type: image_data
                },
                'metadata': {},
            })
            result = None

        stdout = stdout_buf.getvalue().decode('utf-8')
        if stdout != "":
            # Standard output:
            if not silent:
                self.send_response(self.iopub_socket, 'stream', {
                    'name': 'stdout',
                    'text': stdout
                })

        if result is not None:
            # Execution result:
            self.send_response(self.iopub_socket, 'execute_result', {
                'execution_count': self.execution_count,
                'data': {
                    "text/plain": str(result),
                },
                'metadata': {},
            })

        return {'status': 'ok',
                'execution_count': self.execution_count,
                'payload': [],
                'user_expressions': {},
               }
